This the back-end code chanllenge to calculate below mentioned prices for the 'flyer' product.

### Requirement

Property prices are provided in the prices.json file and the 'flyer' product configuration is taken from requestbody.json.

Each property has a price component and is calculated in a unique way as per below.

- _material_ Price is per square mm per 1000 copies.
- _drilling-holes_ Price per drilling hole per 1000 copies.
- _bundles_ Price per bundle.
- _finish_ Price per copy.
- _size_ the size in square mm.

Handled errors when a given configuration is not supported. An error is thrown without calculating the prices.

### Implementation

This is implemented using Node.js Express library. Server runs on port 4000
Created one route '/calculatePrice' to calacuate the product prices.

### How to install this?

- Clone this repository using `git clone ...`
- cd into your project.
- Install dependencies using `npm install`
- Run the server `node index.js`
- Get the product prices from the url `http://localhost:4000/calculatePrice`

  sample output :
  `{ "materialPrice":9355.5, "drillingholesPrice":9, "bundlesPrice":2, "finishPrice":45 }`
