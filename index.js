const express = require("express");

const requestbodyJson = require("./requestbody.json");
const pricesJson = require("./prices.json");
const port = process.env.PORT || 4000;
const app = express();

//console.log("prices:", pricesJson);
app.get("/calculatePrice", (request, response) => {
  let errors = [];
  request = requestbodyJson;
  try {
    const allKeys = Object.keys(request.options);

    allKeys.forEach((key) => {
      switch (key) {
        case "size": {
          if (
            Object.keys(pricesJson.sizes).length > 0 &&
            Object.keys(pricesJson.sizes).includes(requestbodyJson.options[key])
          ) {
          } else {
            errors = [
              ...errors,
              ` Product ${key} - ${requestbodyJson.options[key]} is not in the configuration.`,
            ];
          }
          break;
        }
        case "copies": {
          break;
        }
        default:
          const input = requestbodyJson.options[key].toString();
          if (Object.keys(pricesJson.prices[key]).length > 0) {
            if (Object.keys(pricesJson.prices[key]).includes(input)) {
            } else {
              errors = [
                ...errors,
                `Product ${key} - ${requestbodyJson.options[key]} is not in the configuration`,
              ];
            }
          }
          break;
      }
      //eval(`let ${key}Value =  "${requestbodyJson.options[key]}"`);
    });
    if (errors.length > 0) {
      response.send(errors);
    } else {
      const sizeValue = pricesJson.sizes[requestbodyJson.options["size"]];
      const numOfCopies = requestbodyJson.options["copies"];
      const numOfBundles = requestbodyJson.options["bundles"];
      const finishValue =
        pricesJson.prices["finish"][requestbodyJson.options["finish"]];
      const materialValue =
        pricesJson.prices["material"][requestbodyJson.options["material"]];
      const numOfDrillingHoles =
        pricesJson.prices["drilling_holes"][
          requestbodyJson.options["drilling_holes"]
        ];
      console.log("User Input :", request);
      const materialPrice = materialValue * sizeValue * (numOfCopies / 1000);
      const drillingholesPrice = numOfDrillingHoles * (numOfCopies / 1000);
      const bundlesPrice = numOfBundles * pricesJson.prices["bundles"];
      const finishPrice = numOfCopies * finishValue;
      console.log("materialPrice:", materialPrice);
      console.log("drillingholesPrice:", drillingholesPrice);
      console.log("bundlesPrice:", bundlesPrice);
      console.log("finishPrice:", finishPrice);
      const output = {
        materialPrice,
        drillingholesPrice,
        bundlesPrice,
        finishPrice,
      };
      response.send(output);
    }
  } catch (error) {
    console.log(error);
  }
});

app.listen(port, () => console.log(`Listening on port : ${port}`));
